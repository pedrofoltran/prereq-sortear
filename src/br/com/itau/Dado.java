package br.com.itau;

import java.util.Random;

public class Dado {
    int lados;
    Random aleatorio;

    public Dado(int lados){
        this.lados = lados;
        aleatorio = new Random();
    }

    public int sortear(){
        return aleatorio.nextInt(lados-1)+1;
    }
}
