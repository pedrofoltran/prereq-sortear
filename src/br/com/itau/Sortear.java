package br.com.itau;

import java.util.Random;

public class Sortear {
    public static void main(String[] args) {

        Dado dado = new Dado(6);

        Resultado resultado = Sorteador.sortearGrupo(dado, 3);
        Impressora.imprimir(resultado);

        System.out.println("****************");

        Resultado resultados[] = Sorteador.sortearGrupo(dado, 3, 6);
        Impressora.imprimir(resultados);
    }
}
