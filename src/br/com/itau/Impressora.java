package br.com.itau;

public class Impressora {
    public static void imprimir(Resultado resultado){
        for(int valores : resultado.getResultado()){
            System.out.print(valores + ", ");
        }
        System.out.println(resultado.getSoma());
    }

    public static void imprimir(Resultado resultado[]){
        for(int i=0; i<resultado.length; i++){
            imprimir(resultado[i]);
        }
    }
}
