package br.com.itau;

public class Resultado {
    private int[] numeros;

    public Resultado(int[] numeros){
        this.numeros = numeros;
    }

    public int[] getResultado(){
        return numeros;
    }

    public int getSoma(){
        int acumulador = 0;
        for (int valores : numeros){
            acumulador += valores;
        }

        return acumulador;

    }
}
